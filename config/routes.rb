Rails.application.routes.draw do
  get 'basics/solutions'
  get 'support/indexc'
  get 'election/indexb'




  post 'comment/store'
  get "store", to: "comment#store"
  devise_for :users

  
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)  
  
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html
 

  get "home", to: "home#index"
  # Defines the root path route ("/")
  # root "articles#index"
  root 'basics#solutions'
  
end
